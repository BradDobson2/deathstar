<?php

use App\Api\DeathStar;
use App\Commands;
use Pimple\Container;
use Pimple\Psr11\Container as Psr11Container;

$containerConfig = [
    DeathStar\Client::class => function() {
        return new \App\Api\DeathStar\Client(getenv('DEATH_STAR_API_URL'), getenv('YOUR_NAME'));
    },

    Commands\VulnerabilityScanner::class => function ($container) {
        return new \App\Commands\VulnerabilityScanner(
            $container[\App\Api\DeathStar\Client::class]
        );
    },

    \Symfony\Component\Console\Application::class => function ($container) {
        $application = new \Symfony\Component\Console\Application();

        $application->add(
            $container[Commands\VulnerabilityScanner::class]
        );

        return $application;
    },
];

$pimpleContainer = new Container($containerConfig);

return new Psr11Container($pimpleContainer);
<?php

namespace Tests\Api\DeathStar;

use App\Api\DeathStar\Response as DeathStarResponse;
use App\Api\DeathStar\ResponseFactory;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ResponseFactoryTest extends TestCase
{
    public function testResponseCreationFailsWithInvalidJson()
    {
        $psrResponse = new Response(200, [], 'fjkfdjsklfjsdklfjsdkl');

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('failed to decode json.');

        ResponseFactory::createFromPsrResponse($psrResponse);
    }

    public function testResponseCreationFailsWhenMessageNotAvailableInResponse()
    {
        $psrResponse = new Response(200, [], '{"map": "###   ###"}');

        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('expected message to be available in response.');

        ResponseFactory::createFromPsrResponse($psrResponse);
    }

    public function testResponseCreationFailsWhenMapNotAvailableInResponse()
    {
        $psrResponse = new Response(200, [], '{"message": "a test message."}');

        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('expected map to be available in response.');

        ResponseFactory::createFromPsrResponse($psrResponse);
    }

    public function testResponseCreatedSuccessfullyFromRecognisedResponse()
    {
        $psrResponse = new Response(200, [], '{"map": "###   ###","message": "a test message."}');

        $deathStarResponse = ResponseFactory::createFromPsrResponse($psrResponse);

        $this->assertInstanceOf(DeathStarResponse::class, $deathStarResponse);
        $this->assertEquals(200, $deathStarResponse->getResponseCode());
        $this->assertEquals('a test message.', $deathStarResponse->getMessage());
        $this->assertEquals('###   ###', $deathStarResponse->getMap());
    }
}

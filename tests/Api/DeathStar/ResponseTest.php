<?php

namespace Tests\Api\DeathStar;

use App\Api\DeathStar\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    public function testGetters()
    {
        $response = new Response(200, 'some message', '###   ###');

        $this->assertEquals(200, $response->getResponseCode());
        $this->assertEquals('some message', $response->getMessage());
        $this->assertEquals('###   ###', $response->getMap());
    }

    public function testIsSuccessReturnsFalseForNone200Code()
    {
        $response = new Response(400, 'some message', '###   ###');

        $this->assertFalse($response->isSuccess());
    }

    public function testIsSuccessReturnsTrueFor200Code()
    {
        $response = new Response(200, 'some message', '###   ###');

        $this->assertTrue($response->isSuccess());
    }

    public function testHasCrashedReturnsFalseForNone417Code()
    {
        $response = new Response(422, 'some message', '###   ###');

        $this->assertFalse($response->hasCrashed());
    }

    public function testHasCrashedReturnsTrueFor417Code()
    {
        $response = new Response(417, 'some message', '###   ###');

        $this->assertTrue($response->hasCrashed());
    }
}

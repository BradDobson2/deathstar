<?php

namespace Tests;

use App\CrashMapParser;
use PHPUnit\Framework\TestCase;

class CrashMapParserTest extends TestCase
{
    public function testUnexpectedValueExceptionThrownWhenParsingInvalidCrashMap()
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('no free space detected on crash row.');

        $invalidMap = '#########';

        $crashMapParser = new CrashMapParser($invalidMap);

        $crashMapParser->getClosestFreeSpaceToPositionX(0);
    }

    /**
     * @dataProvider provideClosestFreeSpaceTestData
     */
    public function testGetClosestFreeSpaceToPositionReturnsTheClosestPosition(
        string $crashMap,
        int $position,
        int $expectedClosestPosition
    ) {
        $crashMapParser = new CrashMapParser($crashMap);

        $closestPosition = $crashMapParser->getClosestFreeSpaceToPositionX($position);

        $this->assertEquals($expectedClosestPosition, $closestPosition);
    }

    public function provideClosestFreeSpaceTestData(): array
    {
        return [
            [
                '####  ###',
                1,
                4,
            ],
            [
                '####  *###' . PHP_EOL . '##    ###',
                8,
                5,
            ],
            [
                '####  *###' . PHP_EOL . '##    *###' . PHP_EOL . '#  ######',
                6,
                2,
            ],
        ];
    }
}

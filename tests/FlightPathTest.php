<?php

namespace Tests;

use App\FlightPath;
use PHPUnit\Framework\TestCase;

class FlightPathTest extends TestCase
{
    private $flightPath;

    protected function setUp(): void
    {
        $this->flightPath = new FlightPath();
    }

    public function testFlightPathInitialisedAtExpectedStartingPosition()
    {
        $this->assertEquals(FlightPath::START_POSITION_Y, $this->flightPath->getCurrentPositionY());
        $this->assertEquals(FlightPath::START_POSITION_X, $this->flightPath->getCurrentPositionX());
        $this->assertEquals(false, $this->flightPath->pathHasStarted());
    }

    public function testForwardAdjustsPathAsExpected()
    {
        $this->flightPath->forward();

        $this->assertEquals(true, $this->flightPath->pathHasStarted());
        $this->assertEquals(FlightPath::START_POSITION_Y + 1, $this->flightPath->getCurrentPositionY());
        $this->assertEquals(FlightPath::START_POSITION_X, $this->flightPath->getCurrentPositionX());
        $this->assertEquals([FlightPath::FORWARD], $this->flightPath->getPath());
    }

    public function testLogicExceptionThrownWhenGoingBackWithoutStarting()
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('can\'t go back, path has not started.');

        $this->flightPath->back();
    }

    public function testBackAdjustsPathAsExpected()
    {
        $this->flightPath->forward();
        $this->flightPath->forward();

        $this->assertEquals(FlightPath::START_POSITION_Y + 2, $this->flightPath->getCurrentPositionY());
        $this->assertEquals([FlightPath::FORWARD, FlightPath::FORWARD], $this->flightPath->getPath());

        $this->flightPath->back();

        $this->assertEquals(FlightPath::START_POSITION_Y + 1, $this->flightPath->getCurrentPositionY());
        $this->assertEquals([FlightPath::FORWARD], $this->flightPath->getPath());
    }

    public function testGetCurrentPositionXReturnsExpectedPosition()
    {
        $this->assertEquals(FlightPath::START_POSITION_X, $this->flightPath->getCurrentPositionX());

        $this->flightPath->adjustFlightPathToXPosition(5);

        $this->assertEquals(5, $this->flightPath->getCurrentPositionX());
    }

    public function testGetCurrentPositionYReturnsExpectedPosition()
    {
        $this->assertEquals(FlightPath::START_POSITION_Y, $this->flightPath->getCurrentPositionY());

        $this->flightPath->forward();
        $this->flightPath->forward();

        $this->assertEquals(FlightPath::START_POSITION_Y + 2, $this->flightPath->getCurrentPositionY());
    }

    public function testLogicExceptionThrownWhenAdjustingToAnInvalidPositionX()
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('given position is invalid.');

        $this->flightPath->adjustFlightPathToXPosition(9);
    }

    public function testPositionXIsAdjustedCorrectly()
    {
        $this->flightPath->adjustFlightPathToXPosition(5);

        $this->assertEquals(5, $this->flightPath->getCurrentPositionX());
    }

    public function testPathHasStartedReturnsAsExpected()
    {
        $this->assertFalse($this->flightPath->pathHasStarted());

        $this->flightPath->forward();

        $this->assertTrue($this->flightPath->pathHasStarted());
    }

    public function testGetPathReturnsExpectedPath()
    {
        $this->flightPath->forward();

        $this->assertEquals([FlightPath::FORWARD], $this->flightPath->getPath());

        $this->flightPath->forward();

        $this->flightPath->adjustFlightPathToXPosition(5);

        $this->assertEquals(
            [FlightPath::FORWARD, FlightPath::FORWARD, FlightPath::RIGHT],
            $this->flightPath->getPath()
        );
    }
}

<?php

namespace App;

final class CrashMapParser
{
    private $mapRows = [];

    public function __construct(string $map)
    {
        $mapRows = explode(PHP_EOL, $map);

        $this->mapRows = $mapRows;
    }

    public function getClosestFreeSpaceToPositionX(int $position): int
    {
        $freeSpacePositions = $this->getFreeSpacePositionsOnCrashRow();

        $closestPosition = $freeSpacePositions[0];

        foreach ($freeSpacePositions as $freePosition) {
            if (abs($position - $closestPosition) > abs($freePosition - $position)) {
                $closestPosition = $freePosition;
            }
        }

        return $closestPosition;
    }

    private function getFreeSpacePositionsOnCrashRow(): array
    {
        $freeSpacePositions = [];
        $lastRow = end($this->mapRows);

        if (0 === substr_count($lastRow, ' ')) {
            throw new \UnexpectedValueException('no free space detected on crash row.');
        }

        while (substr_count($lastRow, ' ') > 0) {
            $freeSpacePositions[] = strpos($lastRow, ' ');

            $lastRow = preg_replace('/ /', '#', $lastRow, 1);
        }

        return $freeSpacePositions;
    }
}

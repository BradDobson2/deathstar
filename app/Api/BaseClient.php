<?php

namespace App\Api;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

abstract class BaseClient
{
    private $guzzleClient;

    public function __construct(string $uri)
    {
        $this->guzzleClient = $this->createGuzzleClient($uri);
    }

    protected function get(string $url, array $queryStringParameters = []): ResponseInterface
    {
        try {
            $response = $this->guzzleClient->get(
                $url,
                [] === $queryStringParameters ?: $this->getGuzzleQueryStringConfig($queryStringParameters)
            );
        } catch (RequestException $e) {
            return $e->getResponse();
        }

        return $response;
    }

    private function createGuzzleClient(string $uri): GuzzleClient
    {
        $guzzleConfig = [
            'base_uri' => $uri,
        ];

        return new GuzzleClient($guzzleConfig);
    }

    private function getGuzzleQueryStringConfig(array $queryStringConfig): array
    {
        return ['query' => $queryStringConfig];
    }
}

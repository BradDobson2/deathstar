<?php

namespace App\Api\DeathStar;

use Psr\Http\Message\ResponseInterface;

class ResponseFactory
{
    public static function createFromPsrResponse(ResponseInterface $response): Response
    {
        return new Response(
            $response->getStatusCode(),
            self::getMessageFromResponse($response),
            self::getMapFromResponse($response)
        );
    }

    private static function getMessageFromResponse(ResponseInterface $response): string
    {
        $decodedResponseBody = self::decodeResponseBody($response->getBody());

        if (!isset($decodedResponseBody['message'])) {
            throw new \UnexpectedValueException('expected message to be available in response.');
        }

        return $decodedResponseBody['message'];
    }

    private static function getMapFromResponse(ResponseInterface $response): string
    {
        $decodedResponseBody = self::decodeResponseBody($response->getBody());

        if (!isset($decodedResponseBody['map'])) {
            throw new \UnexpectedValueException('expected map to be available in response.');
        }

        return $decodedResponseBody['map'];
    }

    private static function decodeResponseBody(string $responseBody): array
    {
        $decodedBody = json_decode($responseBody, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \RuntimeException('failed to decode json.');
        }

        return $decodedBody;
    }
}

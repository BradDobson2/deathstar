<?php

namespace App\Api\DeathStar;

use App\Api\BaseClient;
use App\FlightPath;
use Psr\Http\Message\ResponseInterface;

class Client extends BaseClient
{
    private $requesterName;

    public function __construct(string $uri, string $requesterName)
    {
        parent::__construct($uri);

        $this->requesterName = $requesterName;
    }

    public function attemptFlightPath(FlightPath $flightPath): Response
    {
        $flightPathString = implode('', $flightPath->getPath());

        $response = $this->get('/alliance.php', ['name' => $this->requesterName, 'path' => $flightPathString]);

        return $this->createDeathStarResponse($response);
    }

    private function createDeathStarResponse(ResponseInterface $response): Response
    {
        switch ($response->getStatusCode()) {
            case 410:
            case 417:
            case 200:
                return ResponseFactory::createFromPsrResponse($response);
            default:
                throw new \UnexpectedValueException('unrecognised response received');
        }
    }
}

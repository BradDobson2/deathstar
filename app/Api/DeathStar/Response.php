<?php

namespace App\Api\DeathStar;

class Response
{
    private $responseCode;
    private $message;
    private $map;

    public function __construct(int $responseCode, string $message, string $map)
    {
        $this->responseCode = $responseCode;
        $this->message = $message;
        $this->map = $map;
    }

    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getMap(): string
    {
        return $this->map;
    }

    public function isSuccess(): bool
    {
        return 200 === $this->responseCode;
    }

    public function hasCrashed(): bool
    {
        return 417 === $this->responseCode;
    }
}

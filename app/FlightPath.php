<?php

namespace App;

final class FlightPath
{
    const VALID_X_POSITIONS = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const START_POSITION_X = 4;
    const START_POSITION_Y = 0;
    const FORWARD = 'f';
    const RIGHT = 'r';
    const LEFT = 'l';

    private $currentPositionX;
    private $currentPositionY;
    private $path = [];

    public function __construct()
    {
        $this->currentPositionX = self::START_POSITION_X;
        $this->currentPositionY = self::START_POSITION_Y;
    }

    public function forward(): void
    {
        $this->path[] = self::FORWARD;

        $this->currentPositionY++;
    }

    public function back(): void
    {
        if (!$this->pathHasStarted()) {
            throw new \LogicException('can\'t go back, path has not started.');
        }

        array_pop($this->path);

        $this->currentPositionY--;
    }

    public function getCurrentPositionX(): int
    {
        return $this->currentPositionX;
    }

    public function getCurrentPositionY(): int
    {
        return $this->currentPositionY;
    }

    public function adjustFlightPathToXPosition(int $positionX): void
    {
        if (!$this->isValidPositionX($positionX)) {
            throw new \LogicException('given position is invalid.');
        }

        $increments = abs($this->currentPositionX - $positionX);

        if ($positionX > $this->currentPositionX) {
            $this->rightMulti($increments);
        } else {
            $this->leftMulti($increments);
        }
    }

    public function pathHasStarted(): bool
    {
        return [] !== $this->path;
    }

    public function getPath(): array
    {
        return $this->path;
    }

    private function left(): void
    {
        $this->path[] = self::LEFT;

        $this->currentPositionX--;
    }

    private function right(): void
    {
        $this->path[] = self::RIGHT;

        $this->currentPositionX++;
    }

    private function leftMulti(int $increments): void
    {
        for ($i = 0; $increments > $i; $i++) {
            $this->left();
        }
    }

    private function rightMulti(int $increments): void
    {
        for ($i = 0; $increments > $i; $i++) {
            $this->right();
        }
    }

    private function isValidPositionX(int $position): bool
    {
        return in_array($position, self::VALID_X_POSITIONS);
    }
}

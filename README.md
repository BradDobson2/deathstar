#DEATH STAR VULNERABILITY SCANNER

An application developed for the Rebel Alliance to scan the surface of the Death Star for vulnerabilities using droids.

###Considerations
Using a full framework like Laravel may be overkill, so I opted for Symfony Console, in conjunction with Pimple for DI.

###Strategy
1. Keep moving the droid forward until we crash
2. If we crash, parse the map to get the closest free position
3. Adjust the flight path back and navigate to the closest free position
4. Carry on moving forward

###Get Started
```
cp .env.template .env
```

Add your name and the API URI (http://deathstar.victoriaplum.com/) to the env file.

Install dependencies:
```
composer install
```

Run some tests:
```
vendor/bin/phpunit
vendor/bin/phpcs
```

Search for vulnerabilities:
```
./bin/console.php deathstar:vulnerability-scanner
```
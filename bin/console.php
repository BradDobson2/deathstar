#!/usr/bin/env php
<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

$container = require __DIR__ . '/../config/dependencies.php';

$application = $container->get(Application::class);

$application->run();
